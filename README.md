# Piastrix2test

Currently is running here:

https://piastrix2test.herokuapp.com

## Description

This is a little Flask service.

It has the only page for creating payments through Piastrix service.

On the main page there is a simple form to input payment data.
After that (depending on selected currency) servvce does one of the following:

- if **EUR** was selected: service directly redirects user to Piastrix service (protocol PAY)
- if **USD** was selected: service creates a bill and than redirects user to Piastrix service
- if **RUB** was selected: service creates an invoice and than redirects user to Piastrix service

There was added additional page to make all three methods similar to user.
It shows information about payment and redirects user to proper page in 5 seconds.
Or it shows errors if something went wrong on API-requests to Piastrix.

## Install

Service requires *python 3.6* & *virtualenv* to be installed in the system

1. Clone repo to local machine
2. Run commands:

	- `make install`  - this will create venv directory and will install all dependencies
	- `make db`       - sqlite database would be created
	- `make dev`      - flask service will start working

3. Open an `http://localhost:5000` url in your browser
