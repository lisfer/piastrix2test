SHELL := /bin/bash


install:
	if [ ! -d venv ]; then virtualenv -p python3 venv; fi; \
	source ./venv/bin/activate; \
	pip install -r requirements.txt;


dev: install
	export FLASK_APP=main; \
	./venv/bin/flask run --reload --debugger


db: install
	./venv/bin/python create_db.py


shell:
	export FLASK_APP=main; \
	./venv/bin/flask shell


test:
	./venv/bin/pytest tests.py


heroku:
	export FLASK_APP=main; \
	flask run -h 0.0.0.0 -p $(PORT) --reload

