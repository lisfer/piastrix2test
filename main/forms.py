from _sha256 import sha256

from wtforms import Form, StringField, IntegerField, DecimalField, TextAreaField
from wtforms.validators import NumberRange
from wtforms.widgets import HiddenInput, HTMLString
from wtfpeewee.orm import model_form

from main import app
from main.models import PIASTRIX_CURRENCY, Order


class HiddenForm(Form):
    """
    For creating form where all fields are hidden
    """
    url = StringField()
    method = StringField()

    def print_hidden(self):
        """
        Prints all fields like inputs with type="hidden" despite their real type
        :return: HTMLString
        """
        hidden = HiddenInput()
        html = ''
        for name, field in self._fields.items():
            if name not in ['url', 'method']:
                html += hidden(field)
        return HTMLString(html)


class SignedForm(object):
    """
    Mixin that contains common fields for forms with sign attribute
    also creates sign based on required fields
    """

    REQUIRED = ['shop_id', 'shop_order_id']

    sign = StringField()
    shop_id = IntegerField()
    shop_order_id = IntegerField()
    description = TextAreaField()

    def signed_form_init(self, order):
        """
        Alternative init method (we use this class as mixin)
        :param order: order records from database
        :return:
        """
        self.shop_id.data = app.config['PIASTRIX_SHOP_ID']
        self.shop_order_id.data = order.id
        self.sign.data = self.make_sign()
        self.description.data = order.description

    def make_sign(self):
        """
        Makes encoded sign based on required fields.
        :return:
        """
        row = ':'.join([str(self._fields[key]._value()) for key in sorted(self.__class__.REQUIRED)])
        row += app.config['PIASTRIX_KEY']
        return sha256(row.encode('UTF-8')).hexdigest()


class ApiForm(Form):
    """
    Adds aditional method to receive form values like a dict (json structure)
    """

    def get_json(self):
        return {key: field._value() for key, field in self._fields.items()}


class BillFormApi(ApiForm, SignedForm):
    """
    Form for creating bill through REST API request
    """

    REQUIRED = SignedForm.REQUIRED + ['payer_currency', 'shop_currency', 'shop_amount']

    payer_currency = IntegerField()
    shop_currency = IntegerField()
    shop_amount = DecimalField()

    def __init__(self, order):
        super(BillFormApi, self).__init__()
        self.payer_currency.data = PIASTRIX_CURRENCY[order.currency]
        self.shop_currency.data = PIASTRIX_CURRENCY['eur']
        self.shop_amount.data = order.amount
        super(BillFormApi, self).signed_form_init(order)


class PayFormRedirect(HiddenForm, SignedForm):
    """
    Form for redirect to direct pay method
    """

    amount = DecimalField()
    currency = IntegerField()

    REQUIRED = SignedForm.REQUIRED + ['amount', 'currency']

    def __init__(self, order):
        super(PayFormRedirect, self).__init__()
        self.method.data = 'post'
        self.url.data = app.config['DIRECT_PAY_URL']
        self.amount.data = order.amount
        self.currency.data = PIASTRIX_CURRENCY[order.currency]
        super(PayFormRedirect, self).signed_form_init(order)


class IncorrectApiResponse(Exception):
    pass


class InvoiceFormApi(ApiForm, SignedForm):
    """
    Form for creating invoice through REST API
    """

    amount = DecimalField()
    currency = IntegerField()
    payway = StringField()

    REQUIRED = SignedForm.REQUIRED + ['amount', 'currency', 'payway']

    def __init__(self, order):
        super(InvoiceFormApi, self).__init__()
        self.amount.data = order.amount
        self.currency.data = PIASTRIX_CURRENCY[order.currency]
        self.payway.data = app.config['INVOICE_PAYWAY']
        super(InvoiceFormApi, self).signed_form_init(order)


class InvoiceFormRedirect(HiddenForm):
    """
    Form to make redirect to piastrix after invoice was created
    """

    lang = StringField()
    m_curorderid = StringField()
    m_historyid = StringField()
    m_historytm = StringField()
    referer = StringField()

    def __init__(self, in_data):
        super(InvoiceFormRedirect, self).__init__()
        data = in_data.get('data', {})
        try:
            self.url.data = data['url']
            self.method.data = data['method']

            data = data.get('data', {})

            self.lang.data = data['lang']
            self.m_curorderid.data = data['m_curorderid']
            self.m_historyid.data = data['m_historyid']
            self.m_historytm.data = data['m_historytm']
            self.referer.data = data['referer']
        except KeyError:
            app.logger.error(f'Response contains unexpected data structure: {in_data}')
            raise IncorrectApiResponse("Incorrect answer from Piastrix service")


class BillFormRedirect(HiddenForm):
    """
    Form to make redirect to piastrix after bill was created.
    Actually has no any fields - just empty form with url & method == 'get'
    """

    def __init__(self, data):
        super(BillFormRedirect, self).__init__()
        try:
            self.url.data = data.get('data', {})['url']
        except KeyError:
            app.logger.error(f'Response contains unexpected data structure: {data}')
            raise IncorrectApiResponse("Incorrect answer from Piastrix service")
        self.method.data = 'get'


OrderForm = model_form(
    Order,
    exclude=('created_at',),
    field_args={'amount': {'validators': [NumberRange(0.01)]}})
