from datetime import datetime

from main import app
from peewee import Model, DateTimeField, CharField, DecimalField, TextField


PIASTRIX_CURRENCY = dict(eur=978, usd=840, rub=643)


class Order(Model):

    CURRENCIES = (('eur', 'EUR'), ('usd', 'USD'), ('rub', 'RUB'))

    created_at = DateTimeField(default=datetime.now)
    currency = CharField(choices=CURRENCIES)
    amount = DecimalField(decimal_places=2)
    description = TextField(default='')

    class Meta:
        database = app.db

    def __repr__(self):
        return f'<Order: id={self.id}; amount={self.amount} ({self.currency})>'

