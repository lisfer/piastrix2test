import os
from flask import Flask
from playhouse.db_url import connect


def load_config(appl):
    if os.environ.get('HEROKU'):
        appl.config.from_object('main.settings.prod')
    else:
        appl.config.from_object('main.settings.base')


def connect_database(appl):

    def close_db(db):
        def wrap(resp):
            if not db.is_closed():
                db.close()
            return resp
        return wrap

    def connect_db(db):
        def wrap():
            if db.is_closed():
                db.connect()
        return wrap

    appl.db = connect(appl.config['DATABASE_URL'])
    appl.before_request(connect_db(appl.db))
    appl.after_request(close_db(appl.db))


app = Flask(__name__)
load_config(app)
connect_database(app)

from .views import index
