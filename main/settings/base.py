from logging.config import dictConfig

dictConfig({
    'version': 1,
    'root': {
        'level': 'INFO',
    }
})


DATABASE_URL = 'sqlite:///piastrix.db'
TEMPLATES_AUTO_RELOAD = True
PIASTRIX_KEY = "SecretKey01"
PIASTRIX_SHOP_ID = 5
INVOICE_PAYWAY = 'payeer_rub'
DIRECT_PAY_URL = "https://pay.piastrix.com/ru/pay"
CREATE_BILL_URL = "https://core.piastrix.com/bill/create"
CREATE_INVOICE_URL = "https://core.piastrix.com/invoice/create"
DEBUG = True