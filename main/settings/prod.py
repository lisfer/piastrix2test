import os
from .base import *


DATABASE_URL = os.environ.get('DATABASE_URL')
DEBUG = False