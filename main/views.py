import requests
from flask import render_template, request

from main import app
from main.models import Order
from main.forms import PayFormRedirect, IncorrectApiResponse, InvoiceFormRedirect, OrderForm, BillFormApi, \
    InvoiceFormApi, BillFormRedirect


@app.route('/')
def index():
    app.logger.info(f'Get order form')
    order_form = OrderForm()
    return render_template('index.html', order_form=order_form)


@app.route('/', methods=['POST'])
def create_order():
    app.logger.info(f'Post order form')
    order_form = OrderForm(request.form)
    if order_form.validate():
        order = Order()
        order_form.populate_obj(order)
        order.save()
        app.logger.info(f'Saved order: {order}')
        if order.currency == 'eur':
            return pay_redirect(order)
        elif order.currency == 'usd':
            return api_request_redirect(
                app.config['CREATE_BILL_URL'], BillFormApi, BillFormRedirect, order, 'Bill')
        else:
            return api_request_redirect(
                app.config['CREATE_INVOICE_URL'], InvoiceFormApi, InvoiceFormRedirect, order, 'Invoice')
    app.logger.info(f'OrderForm contains errors: {order_form.errors}')
    return render_template('index.html', order_form=order_form)


def pay_redirect(order):
    app.logger.info('Redirect to direct pay')
    form = PayFormRedirect(order)
    return render_template('pay_redirect.html', redirect_form=form, order=order, payment_type='Direct Pay')


def api_request(url, data):
    resp = requests.post(url, json=data)
    try:
        data = resp.json()
    except ValueError:
        app.logger.error(f'Response ({resp.text()}) could not be parsed as JSON')
        raise IncorrectApiResponse("Incorrect answer from Piastrix service")
    if data['error_code'] != 0:
        app.logger.error(f'Response contains error: {data}')
        raise IncorrectApiResponse(f"({resp.json()['error_code']}) {resp.json()['message']}")
    return data


def api_request_redirect(url, api_form, redirect_form, order, payment_type):
    app.logger.info(f'API request: {payment_type}')
    a_form = api_form(order)
    try:
        data = api_request(url, a_form.get_json())
        r_form = redirect_form(data)
    except IncorrectApiResponse as e:
        return render_template('pay_redirect.html', error=e)

    app.logger.info(f'Redirect to: {payment_type}')
    return render_template('pay_redirect.html', redirect_form=r_form, order=order, payment_type=payment_type)
