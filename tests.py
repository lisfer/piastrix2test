import re
from _sha256 import sha256

import pytest

from main.forms import HiddenForm, InvoiceFormRedirect, IncorrectApiResponse, BillFormRedirect, PayFormRedirect, \
    BillFormApi, InvoiceFormApi
from main.models import Order, PIASTRIX_CURRENCY
from main import app


def test_print_hidden_form_empty():
    hform = HiddenForm()
    assert hform.print_hidden() == ''


def get_input_values(row):
    """
    parses names and values of <input> fields from the row
    :param row:
    :return:
    """
    data_out = {}
    for input_field in row.split('<input'):
        if input_field:
            data_out[re.findall('name="(\w+)"', input_field)[0]] = re.findall('value="([A-Za-z0-9\.]+)"', input_field)[0]
    return data_out


def test_print_hidden_invoice_form():
    data = {'data': {
        'data': {'lang': 'ru', 'm_curorderid': 1, 'm_historyid': 2, 'm_historytm': 3, 'referer': 'referer'},
        'url': 'url',
        'method': 'get'}}

    hform = InvoiceFormRedirect(data)
    data_out = get_input_values(hform.print_hidden())

    assert data_out['lang'] == 'ru'
    assert data_out['m_curorderid'] == '1'
    assert data_out['m_historyid'] == '2'
    assert data_out['m_historytm'] == '3'
    assert data_out['referer'] == 'referer'

    assert len(data_out) == 5
    assert hform.url.data == 'url'
    assert hform.method.data == 'get'


def test_invoice_form_fail():
    with pytest.raises(IncorrectApiResponse) as e:
        hform = InvoiceFormRedirect(dict())
        assert e.message == "Incorrect answer from Piastrix service"


def test_bill_form_fail():
    hform = BillFormRedirect({'data': {'url': 'some_url'}})
    assert hform.url.data == 'some_url'


def test_bill_form_fail():
    with pytest.raises(IncorrectApiResponse) as e:
        hform = BillFormRedirect(dict())
        assert e.message == "Incorrect answer from Piastrix service"


def test_make_sign():
    order = Order(amount=10.0, id=1, currency='eur')
    form = PayFormRedirect(order)
    expected_row = f"10.00:{PIASTRIX_CURRENCY[order.currency]}:{app.config['PIASTRIX_SHOP_ID']}:{order.id}{app.config['PIASTRIX_KEY']}"
    assert sha256(expected_row.encode('UTF-8')).hexdigest() == form.make_sign()


def test_payform():
    order = Order(amount=20.0, id=2, currency='usd', description='description')
    form = PayFormRedirect(order)
    data_out = get_input_values(form.print_hidden())

    assert data_out['shop_id'] == str(app.config['PIASTRIX_SHOP_ID'])
    assert data_out['shop_order_id'] == '2'
    assert data_out['description'] == 'description'

    expected_sign = f"20.00:{PIASTRIX_CURRENCY[order.currency]}:{app.config['PIASTRIX_SHOP_ID']}:{order.id}{app.config['PIASTRIX_KEY']}"
    assert data_out['sign'] == sha256(expected_sign.encode('UTF-8')).hexdigest()
    assert data_out['amount'] == '20.00'
    assert data_out['currency'] == str(PIASTRIX_CURRENCY[order.currency])

    assert form.url.data == app.config['DIRECT_PAY_URL']
    assert form.method.data == 'post'


def test_billform():
    order = Order(amount=30.0, id=3, currency='usd', description='description')
    form = BillFormApi(order)

    data_out = form.get_json()
    assert data_out['shop_id'] == str(app.config['PIASTRIX_SHOP_ID'])
    assert data_out['shop_order_id'] == '3'
    assert data_out['description'] == 'description'

    expected_sign = f"{PIASTRIX_CURRENCY[order.currency]}:30.00:{PIASTRIX_CURRENCY['eur']}:{app.config['PIASTRIX_SHOP_ID']}:{order.id}{app.config['PIASTRIX_KEY']}"
    assert data_out['sign'] == sha256(expected_sign.encode('UTF-8')).hexdigest()

    assert data_out['payer_currency'] == str(PIASTRIX_CURRENCY[order.currency])
    assert data_out['shop_currency'] == str(PIASTRIX_CURRENCY['eur'])
    assert data_out['shop_amount'] == '30.00'


def test_invoiceform():
    order = Order(amount=40.0, id=4, currency='usd', description='description')
    form = InvoiceFormApi(order)
    data_out = form.get_json()

    assert data_out['shop_id'] == str(app.config['PIASTRIX_SHOP_ID'])
    assert data_out['shop_order_id'] == '4'
    assert data_out['description'] == 'description'

    expected_sign = f"40.00:{PIASTRIX_CURRENCY[order.currency]}:{app.config['INVOICE_PAYWAY']}:{app.config['PIASTRIX_SHOP_ID']}:{order.id}{app.config['PIASTRIX_KEY']}"
    assert data_out['sign'] == sha256(expected_sign.encode('UTF-8')).hexdigest()
    assert data_out['amount'] == '40.00'
    assert data_out['currency'] == str(PIASTRIX_CURRENCY[order.currency])
    assert data_out['payway'] == app.config['INVOICE_PAYWAY']
